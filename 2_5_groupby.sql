--Wybierz niepowtarzalne wartości kolumny City dla tabeli Invoice 

SELECT DISTINCT "City" FROM "Invoice";

--Znajdź najwyższą wartość kolumny Total tabeli Invoice

SELECT MAX("Total") FROM "Invoice";

--Znajdź najniższą wartość kolumny Total tabeli Invoice

SELECT MIN("Total") FROM "Invoice";

--Znajdź średnią wartość kolumny Total tabeli Invoice

SELECT AVG("Total") FROM "Invoice";

--Znajdź średnią wartość kolumny Total tabeli Invoice zaokrąglona do 2 miejsc

SELECT round(AVG("Total"),2) AS "Average" FROM "Invoice";

--Znajdź sumę wartości faktur w podziale na kraje

SELECT sum("Total"), "BillingCountry" FROM "Invoice" GROUP BY "BillingCountry";


--Znajdź sumę wartości faktur w podziale na kraje, sortowane malejąco według sumy

SELECT sum("Total"), "BillingCountry" FROM "Invoice" GROUP BY "BillingCountry"
ORDER BY sum DESC;

--Znajdź sumę wartości faktur w podziale na kraje i listę niepowtarzalnych numerów klientów, sortowane malejąco według sumy

SELECT sum("Total"), "BillingCountry", array_agg(DISTINCT "CustomerId") AS "Customers" FROM "Invoice" GROUP BY "BillingCountry"
ORDER BY sum DESC;




