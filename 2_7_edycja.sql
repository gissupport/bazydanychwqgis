--Ustawienie "MediaTypeId" na 5 dla wszystkich wierszy w tabeli Track

UPDATE "Track" SET "MediaTypeId" = 5;

--Usuniecie wszystkich wierszy z tabeli PlaylistTrack

DELETE FROM "PlaylistTrack";

--Sprawdzenie liczby klientów przypisanych do pracownika

SELECT "SupportRepId", count(*) FROM "Customer"
GROUP BY "SupportRepId" ORDER BY count;

--Zmiana przypisanego pracownika do klientów mających opiekuna o ID=5

UPDATE "Customer" SET "SupportRepId" = 4 WHERE "SupportRepId" = 5;

--Usunięcie pracownika o ID=5

DELETE FROM "Employee" WHERE "EmployeeId" = 5;