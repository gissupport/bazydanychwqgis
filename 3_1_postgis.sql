--Aktywacja PostGIS w bazie

CREATE EXTENSION postgis;

--Sprawdzenie, czy PostGIS jest aktywny

SELECT postgis_full_version();

--Sprawdzenie układu współrzędnych w tabeli

SELECT DISTINCT ST_SRID(geom) FROM koleje;

--Transformacja w locie do układu WGS84

SELECT gml_id, ST_Transform(geom,4326) AS geom FROM koleje;

--Wybranie tylko dróg krajowych

SELECT * FROM drogi WHERE "katZarzadzania" = 'K';