--znajdź drogi znajdujące się wewnątrz powiatu głubczyckiego

SELECT a.* FROM drogi a JOIN granice_administracyjne b
ON (ST_Within(a.geom, b.geom) AND b.nazwa='głubczycki');

--znajdź drogi przecinające granicę powiatu głubczyckiego

SELECT a.* FROM drogi a JOIN granice_administracyjne b
ON (ST_Crosses(a.geom, b.geom)
AND b.nazwa='głubczycki');

--znajdź lasy znajdujące się w całości na terenie parków krajobrazowych

SELECT a.* FROM lasy a JOIN parki_krajobrazowe b
ON ST_Within(a.geom, b.geom);

--znajdź lasy mające wspólną powierzchnię z parkami krajobrazowymi, ale nie znajdujące się w całości wewnątrz

SELECT a.* FROM lasy a JOIN parki_krajobrazowe b
ON ST_Overlaps(a.geom, b.geom);

--znajdź lasy mające dowolną relację przestrzenną z parkami krajobrazowymi 

SELECT a.* FROM lasy a JOIN parki_krajobrazowe b
ON ST_Intersects(a.geom, b.geom);

--znajdź powiaty, które stykają się z powiatem głubczyckim

SELECT a.* FROM granice_administracyjne a JOIN granice_administracyjne b
ON (ST_Touches(a.geom, b.geom) AND a.rodzaj='Pow' AND b.nazwa='głubczycki');

--znajdź miejscowości położone na terenie lub najdalej 1 km od granic parków krajobrazowych

SELECT a.*, b.nazwa AS park FROM miejscowosci a
JOIN parki_krajobrazowe b
ON ST_Dwithin(a.geom, b.geom, 1000);

--znajdź miejscowości położone na terenie lub najdalej 1 km od granic parków krajobrazowych, oblicz odległość zaokrągloną do 2 miejsc
--po przecinku

SELECT a.*, b.nazwa AS park, round(st_distance(a.geom,b.geom)::numeric,2) AS dystans FROM miejscowosci a
JOIN parki_krajobrazowe b
ON ST_Dwithin(a.geom, b.geom, 1000);


