--Znajdź miasto, adres, wartość dla faktur w których miasto zaczyna się na B

SELECT "BillingCity", "BillingAddress", "Total" FROM "Invoice" WHERE "BillingCity" LIKE 'B%';

--Znajdź miasto, adres, wartość dla faktur w których adres kończy się na Street

SELECT "BillingCity", "BillingAddress", "Total" FROM "Invoice" WHERE "BillingAddress" LIKE '%Street';

--Znajdź miasto, adres, wartość dla faktur w których adres zawiera słowo Berry

SELECT "BillingCity", "BillingAddress", "Total" FROM "Invoice" WHERE "BillingAddress" LIKE '%Berry%';

--Znajdź miasto, adres, wartość dla faktur w których adres zawiera słowo Berry z dowolną wielkością liter

SELECT "BillingCity", "BillingAddress", "Total" FROM "Invoice" WHERE "BillingAddress" ILIKE '%berry%';

--Znajdź stan i nazwę kraju przekształconą do wielkich liter

SELECT "State", upper("Country") AS "Billing Country" FROM "Customer";

--Znajdź stan i nazwę kraju przekształconą do małych liter

SELECT "State", lower("Country") AS "Billing Country" FROM "Customer";

--Przekształć tekst 'CZECH REPUBLIC' na pisownię właściwa dla nazw własnych

SELECT initcap('CZECH REPUBLIC');

--Wydobądź domenę z adresów e-mail

SELECT "State", lower("Country") AS "Billing Country",
"Email", split_part("Email",'@',2) AS "Domain"
FROM "Customer";



