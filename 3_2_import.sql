--import danych ogr2ogr (komenda wykonana w wierszu polecenia OSGeo4W Shell - NIE w bazie danych)
--SET PGCLIENTENCODING=WIN1250
--ogr2ogr -f PostgreSQL PG:"host=localhost port=5432 user=postgres password=postgis dbname=gis" -nln rezerwaty -lco GEOMETRY_NAME=geom -lco FID=id -nlt PROMOTE_TO_MULTI C:\Users\User\Desktop\BAZY\Rezerwaty\RezerwatyPolygon.shp


--tworzenie tabeli na punkty adresowe z OpenAddresses

CREATE TABLE punkty_adresowe(
lon float, lat float, number varchar, street varchar, unit varchar, city varchar, district varchar, region varchar, postcode varchar,
id varchar, hash varchar);

--dodanie kolumny geometrii (na filmie jest dodana przez narzędzie graficzne)

ALTER TABLE punkty_adresowe ADD COLUMN geom geometry;

--wypelnienie kolumny geometrii

UPDATE punkty_adresowe SET geom = ST_SetSRID(ST_MakePoint(lon,lat),4326);
