--Utworzenie użytkownika jan_kowalski z hasłem postgis (na filmie utworzony w narzędziu graficznym)

CREATE USER jan_kowalski WITH PASSWORD 'postgis';

--przyznanie uprawnień do odczytu na tabelę "drogi" dla użytkownika jan_kowalski

GRANT SELECT ON drogi TO jan_kowalski;

--przyznanie uprawnień do odczytu na wszystkie tabele schematu public dla użytkownika jan_kowalski

GRANT SELECT ON ALL TABLES IN SCHEMA public TO jan_kowalski;

--przyznanie wszystkich uprawnień do tabeli drogi dla użytkownika jan_kowalski

GRANT ALL ON drogi TO jan_kowalski;

--wersja alternatywna

GRANT SELECT, INSERT, UPDATE, DELETE ON koleje TO jan_kowalski;

--przyznanie uprawnień do sekwencji koleje_id_seq (konieczne do działania tworzenia nowych obiektów)

GRANT USAGE ON SEQUENCE drogi_id_seq TO jan_kowalski;
GRANT USAGE ON SEQUENCE koleje_id_seq TO jan_kowalski;

--odebranie uprawnień do edycji

REVOKE INSERT, UPDATE, DELETE ON koleje FROM jan_kowalski;