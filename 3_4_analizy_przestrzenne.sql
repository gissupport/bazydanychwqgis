--bufor 5 km od dróg

SELECT gml_id, "katZarzadzania", ST_Buffer(geom,5000) AS geom
 FROM drogi;
 
--bufor 1 km od wód powierzchniowych

SELECT gml_id, ST_Buffer(geom,1000) AS geom
 FROM wody_powierzchniowe;
 
--bufor równy liczbie mieszkańców od miejscowości

SELECT gml_id, nazwa, ST_Buffer(geom,"liczbaMieszkancow") AS geom
 FROM miejscowosci;
 
 --iloczyn lasów i parków krajobrazowych

SELECT a.gml_id, b.nazwa, st_intersection(a.geom, b.geom)
 from lasy a, parki_krajobrazowe b
 where st_intersects(a.geom, b.geom);

--przecięcie parków krajobrazowych z lasami, scalenie fragmentów lasów i zsumowanie powierzchni leśnej dla każdego parku

SELECT b.nazwa, st_union(st_intersection(a.geom, b.geom)), st_area(st_union(st_intersection(a.geom, b.geom)))/10000 AS powierzchnia_lesna
 from lasy a, parki_krajobrazowe b
 where st_intersects(a.geom, b.geom)
 group by b.nazwa;
 
--agregacja gmin do powiatów

SELECT "idTerytJednostkiNadrzednej", st_union(geom)
 FROM granice_administracyjne
 WHERE rodzaj like 'G%'
 GROUP BY "idTerytJednostkiNadrzednej";
 
--wycięcie lasów z powierzchni powiatu oleskiego

SELECT a.nazwa, a.gml_id, ST_Area(a.geom) / 1000000 AS powierzchnia, ST_Difference(a.geom, las.st_union), ST_Area( ST_Difference(a.geom, las.st_union))/1000000 AS powierzchnia_nielesna
 FROM granice_administracyjne a, 
 (SELECT ST_Union(a.geom) FROM lasy a, granice_administracyjne b
 WHERE ST_Intersects(a.geom, b.geom) AND b.nazwa='oleski') las
 WHERE a.nazwa = 'oleski';