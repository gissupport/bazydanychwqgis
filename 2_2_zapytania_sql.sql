--Pobierz nazwę firmy i adres z tabeli Customer gdzie miasto to Praga
SELECT "Company", "Address" FROM "Customer" WHERE "City" = 'Prague';

--Pobierz nazwę firmy, adres, kod pocztowy, telefon, e-mail z tabeli Customer gdzie miasto to Praga
SELECT "Company", "Address", "PostalCode", "Phone", "Email" FROM "Customer" WHERE "City" = 'Prague';

--Pobierz datę, adres, kraj, wartość z tabeli Invoice gdzie wartość jest wieksza od 10.5
SELECT "InvoiceDate", "BillingAddress", "BillingCountry", "Total" FROM "Invoice" WHERE "Total" > 10.5;

--Pobierz datę, adres, kraj, wartość z tabeli Invoice gdzie wartość jest większa od 10.5, sortując rosnąco wg wartości
SELECT "InvoiceDate", "BillingAddress", "BillingCountry", "Total" FROM "Invoice" WHERE "Total" > 10.5 ORDER BY "Total";

--Pobierz datę, adres, kraj, wartość z tabeli Invoice gdzie wartość jest większa od 10.5, sortując malejąco wg wartości
SELECT "InvoiceDate", "BillingAddress", "BillingCountry", "Total" FROM "Invoice" WHERE "Total" > 10.5 ORDER BY "Total" DESC;

--Pobierz datę, adres, kraj, wartość z tabeli Invoice gdzie wartość jest większa od 10.5 i kraj to USA, sortując malejąco wg wartości
SELECT "InvoiceDate", "BillingAddress", "BillingCountry", "Total" FROM "Invoice" WHERE
"Total" > 10.5 AND "BillingCountry" = 'USA' ORDER BY "Total" DESC;

--Pobierz datę, adres, kraj, wartość z tabeli Invoice gdzie wartość jest większa od 10.5 i kraj to USA lub Czechy, sortując malejąco wg wartości
SELECT "InvoiceDate", "BillingAddress", "BillingCountry", "Total" FROM "Invoice" WHERE
"Total" > 10.5 AND ("BillingCountry" = 'USA' OR "BillingCountry" = 'Czech Republic') ORDER BY "Total" DESC;

--Pobierz datę, adres, kraj, wartość z tabeli Invoice gdzie wartość jest mniejsza od 13.86 i kraj to USA lub Czechy, sortując malejąco wg wartości
SELECT "InvoiceDate", "BillingAddress", "BillingCountry", "Total" FROM "Invoice" WHERE
"Total" < 13.86 AND ("BillingCountry" = 'USA' OR "BillingCountry" = 'Czech Republic') ORDER BY "Total" DESC;

--Pobierz datę, adres, kraj, wartość z tabeli Invoice gdzie wartość jest mniejsza lub równa 13.86
--i kraj to USA lub Czechy, sortując malejąco wg wartości
--maksymalnie 10 wierszy
SELECT "InvoiceDate", "BillingAddress", "BillingCountry", "Total" FROM "Invoice" WHERE
"Total" <= 13.86 AND ("BillingCountry" = 'USA' OR "BillingCountry" = 'Czech Republic') ORDER BY "Total" DESC LIMIT 10;

--Pobierz datę, adres, kraj, wartość z tabeli Invoice gdzie wartość jest większa od 13.86
--i kraj jest inny niż USA, sortując malejąco wg wartości
--maksymalnie 10 wierszy
SELECT "InvoiceDate", "BillingAddress", "BillingCountry", "Total" FROM "Invoice" WHERE
"Total" > 13.86 AND "BillingCountry" != 'USA' ORDER BY "Total" DESC LIMIT 10;

--Pobierz datę, adres, kraj, wartość z tabeli Invoice gdzie wartość jest większa od 13.86
--i kraj to jeden z: USA, Austria, Czechy, Węgry, sortując malejąco wg wartości
--maksymalnie 10 wierszy
SELECT "InvoiceDate", "BillingAddress", "BillingCountry", "Total" FROM "Invoice" WHERE
"Total" > 13.86 AND "BillingCountry" IN('USA', 'Austria', 'Czech Republic', 'Hungary') ORDER BY "Total" DESC LIMIT 10;

--Pobierz datę, adres, kraj, wartość z tabeli Invoice gdzie wartość jest większa od 13.86
--i kraj nie jest jednym z : USA, Austria, Czechy, Węgry, sortując malejąco wg wartości
--maksymalnie 10 wierszy
SELECT "InvoiceDate", "BillingAddress", "BillingCountry", "Total" FROM "Invoice" WHERE
"Total" > 13.86 AND "BillingCountry" NOT IN('USA', 'Austria', 'Czech Republic', 'Hungary') ORDER BY "Total" DESC LIMIT 10;

--Pobierz datę, adres, kraj, wartość z tabeli "Invoice" gdzie kraj to USA, sortując malejąco według daty, maksymalnie 10 wierszy (10 najnowszych)

SELECT "InvoiceDate", "BillingAddress", "BillingCountry", "Total" FROM "Invoice" WHERE
"BillingCountry" = 'USA' ORDER BY "InvoiceDate" DESC LIMIT 10;


--Pobierz datę, adres, kraj, wartość z tabeli "Invoice" gdzie kraj to USA, sortując malejąco według wartosci a nastepnie daty,
--maksymalnie 10 wierszy

SELECT "InvoiceDate", "BillingAddress", "BillingCountry", "Total" FROM "Invoice" WHERE
"BillingCountry" = 'USA' ORDER BY "Total" DESC "InvoiceDate" LIMIT 10;

