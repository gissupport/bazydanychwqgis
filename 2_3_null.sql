--Pobierz wszystkie kolumny z tabeli Customer gdzie Company jest puste

SELECT * FROM "Customer" WHERE "Company" IS NULL;

--Pobierz wszystkie kolumny z tabeli Customer gdzie Company nie jest puste

SELECT * FROM "Customer" WHERE "Company" IS NOT NULL;

--Pobierz wszystkie kolumny z tabeli Customer gdzie Company nie jest z listy Google, Microsoft, Apple
-- i nie jest NULL

SELECT * FROM "Customer" WHERE "Company" NOT IN ('Google Inc.', 'Microsoft Corporation', 'Apple Inc.');

--Pobierz wszystkie kolumny z tabeli Customer gdzie Company nie jest z listy Google, Microsoft, Apple
-- lub jest NULL

SELECT * FROM "Customer" WHERE "Company" NOT IN ('Google Inc.', 'Microsoft Corporation', 'Apple Inc.')
OR "Company" IS NULL;

--Pobierz wszystkie kolumny z tabeli Customer sortując malejąco po kolumnie Company. NULL na początku

SELECT * FROM "Customer" ORDER BY "Company" DESC;

--Pobierz wszystkie kolumny z tabeli Customer sortując malejąco po kolumnie Company. NULL na końcu

SELECT * FROM "Customer" ORDER BY "Company" DESC NULLS LAST;


