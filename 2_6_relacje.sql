--Połącz dane faktury z danymi klienta, wykorzystując klucz CustomerId
--wariant z klauzulą WHERE

SELECT a."InvoiceId", a."InvoiceDate", a."BillingAddress",a."BillingCity", a."Total", a."CustomerId" as a_customer,
b."CustomerId" as b_customer, b."FirstName", b."LastName", b."Company"
FROM "Invoice", a "Customer" b
WHERE a."CustomerId" = b."CustomerId";

--Połącz dane faktury z danymi klienta, wykorzystując klucz CustomerId
--wariant z klauzulą JOIN

SELECT a."InvoiceId", a."InvoiceDate", a."BillingAddress",a."BillingCity", a."Total", a."CustomerId" as a_customer,
b."CustomerId" as b_customer, b."FirstName", b."LastName", b."Company"
FROM "Invoice" a 
JOIN "Customer" b
ON a."CustomerId" = b."CustomerId";


--Połącz dane faktury z danymi klienta, wykorzystując klucz CustomerId
--wariant z klauzulą JOIN i dodatkowym filtrowaniem

SELECT a."InvoiceId", a."InvoiceDate", a."BillingAddress",a."BillingCity", a."Total", a."CustomerId" as a_customer,
b."CustomerId" as b_customer, b."FirstName", b."LastName", b."Company"
FROM "Invoice" a 
JOIN "Customer" b
ON a."CustomerId" = b."CustomerId"
WHERE b."Company" IS NOT NULL;

--Połącz dane utworów z danymi faktur - tylko te utwory, które znalazły sie na jakiejkolwiek fakturze

SELECT a."Name", b."InvoiceId"
FROM "Track" a
JOIN "InvoiceLine" b
ON a."TrackId" = b."TrackId"
ORDER BY a."Name" DESC;

--Połącz dane utworów z danymi faktur - wszystkie utwory, także nigdy niesprzedane

SELECT a."Name", b."InvoiceId"
FROM "Track" a
LEFT JOIN "InvoiceLine" b
ON a."TrackId" = b."TrackId"
ORDER BY a."Name" DESC;

--Połącz dane pracowników z ich przełożonymi (self join)

SELECT a."FirstName", a."LastName", b."FirstName" as "ManagerFirstName", b."LastName" as "ManagerLastName",
FROM "Employee" a
LEFT JOIN "Employee" b
ON a."ReportsTo" = b."EmployeeId";






